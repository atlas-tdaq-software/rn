# rn (Run Number)

## tdaq-09-01-00

Jira: [ADTCC-242](https://its.cern.ch/jira/browse/ADTCC-242) 

Store start of run timestamp and run duration with nanoseconds resolution.
Store TDAQ release name.
If oks repository is used, store oks config version into run number database and tag oks repository by run number / partition name.
