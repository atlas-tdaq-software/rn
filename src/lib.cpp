#include <algorithm>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/time.h>

#include <mutex>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/AttributeSpecification.h>
#include <CoralBase/Exception.h>
#include <CoralBase/TimeStamp.h>
#include <CoralKernel/Context.h>

#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ITableDescription.h>
#include <RelationalAccess/ITableDataEditor.h>
#include "RelationalAccess/IOperationWithQuery.h"
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/SchemaException.h>

#include <system/User.h>
#include <system/Host.h>

#include <oks/kernel.h>

#include <dal/util.h>

#include "rn/rn.h"

static std::mutex s_run_number_mutex;

namespace tdaq {

  static std::string
  ts2str(const coral::TimeStamp& t)
  {
    char buf[] = "yyyymmddThhmmss";

    std::sprintf(buf, "%4.4d%2.2d%2.2dT%2.2d%2.2d%2.2d", t.year(), t.month(), t.day(), t.hour(), t.minute(), t.second());

    return std::string(buf);
  }

class ConnectionGuard {
  
  public:
  
    ConnectionGuard(RunNumber * n) : m_rn(n) { ; }
    ~ConnectionGuard() { m_rn->clean_connection(); }


  private:

    RunNumber * m_rn;
};


const char * RunNumber::s_table_name             = "RUNNUMBER";
const char * RunNumber::s_name_column            = "NAME";
const char * RunNumber::s_run_number_column      = "RUNNUMBER";
const char * RunNumber::s_start_at_column        = "STARTAT";
const char * RunNumber::s_started_at_column      = "STARTEDAT";
const char * RunNumber::s_duration_column        = "DURATION";
const char * RunNumber::s_lasted_column          = "LASTED";
const char * RunNumber::s_release_column         = "RELEASE";
const char * RunNumber::s_created_by_column      = "CREATEDBY";
const char * RunNumber::s_host_column            = "HOST";
const char * RunNumber::s_partition_name_column  = "PARTITIONNAME";
const char * RunNumber::s_config_schema_column   = "CONFIGSCHEMA";
const char * RunNumber::s_config_data_column     = "CONFIGDATA";
const char * RunNumber::s_config_version_column  = "CONFIGVERSION";
const char * RunNumber::s_config_name_column     = "CONFIGNAME";
const char * RunNumber::s_comments_column        = "COMMENTS";

struct TagRepository
{
  const unsigned long m_run_number;
  const std::string m_partition_name;
  const std::string m_config_version;

  TagRepository(unsigned long num, const std::string partition_name, const std::string& version) :
    m_run_number(num), m_partition_name(partition_name), m_config_version(version)
  {
    ;
  }

  void
    operator()()
    {
      try
        {
          std::ostringstream tag;
          tag << 'r' << m_run_number << '@' << m_partition_name;
          OksKernel k(false, false, false, true, m_config_version.c_str());
          k.tag_repository(tag.str());
        }
      catch (const oks::exception& ex)
        {
          ers::error(rn::TagConfigRepositoryFailure(ERS_HERE, ex.what()));
        }
    }
};


RunNumber::RunNumber(const std::string& connect, const std::string& partition, unsigned long existing_run_number) :
m_run_number         (0),
m_session            (0),
m_connection_service (0)
{

    // get connect string

  std::string connect_str(connect);
  
  if(connect_str.empty()) {
    if(const char * env = getenv("TDAQ_RUN_NUMBER_CONNECT")) { connect_str = env; }
  }

  if(connect_str.empty()) {
    throw (rn::NoParameter(ERS_HERE, "connect string", "TDAQ_RUN_NUMBER_CONNECT"));
  }


    // get partition name

  m_partition_name = partition;

  if(m_partition_name.empty()) {
    if(const char * env = getenv("TDAQ_PARTITION")) { m_partition_name = env; }
  }

  if(m_partition_name.empty()) {
    throw (rn::NoParameter(ERS_HERE, "partition name", "TDAQ_PARTITION"));
  }

  if(getenv("TDAQ_DB_VERSION") && getenv("TDAQ_DB_REPOSITORY") && !getenv("TDAQ_DB_USER_REPOSITORY")) {
    m_config_version = daq::core::get_config_version(m_partition_name);
  }

    // parse connect string
    //
    // FORMAT:  "db-name:db-owner:db-connect-string", e.g.:
    //          "nightly:tdaq_writer:oracle://devdb/tdaq"


  std::string::size_type idx = connect_str.find(':');

  if(idx == std::string::npos) {
    throw (rn::BadConnectParameter(ERS_HERE, "db-name", connect_str.c_str()));
  }

  m_db_name = connect_str.substr(0, idx);

  std::string::size_type idx2 = connect_str.find(':',idx+1);

  if(idx2 == std::string::npos) {
    throw (rn::BadConnectParameter(ERS_HERE, "db-owner", connect_str.c_str()));
  }

  m_db_owner = connect_str.substr(idx + 1, idx2 - idx - 1);

  m_db_connect = connect_str.substr(idx2 + 1);

  ERS_DEBUG(2, "use parameters: "
               "partition = \'" << m_partition_name << "\', "
	       "db-name = \'" << m_db_name << "\', "
	       "db-owner = \'" << m_db_owner << "\', "
	       "db-connect = \'" << m_db_connect << "\'." );


  try {
    std::lock_guard<std::mutex> scoped_lock(s_run_number_mutex);

    m_session = get_session(m_db_connect, static_cast<int>(coral::Update), m_connection_service);
    ConnectionGuard __cg__(this);  // close connection on constructor exit

    coral::ITable * m_table = &m_session->schema(m_db_owner).tableHandle( s_table_name );

    coral::AttributeList data;
    m_table->dataEditor().rowBuffer( data );


      // "reconnect" to existing run number record

    if(existing_run_number) {
      ERS_DEBUG(3, "Reading description of run number " << existing_run_number << " database \'" << m_db_name << "\'...");
      std::unique_ptr< coral::IQuery > query( m_table->newQuery() );
      query->setRowCacheSize( 1 );
      coral::AttributeList conditionData;
      conditionData.extend<std::string>( "nm" );
      conditionData["nm"].data<std::string>() = m_db_name;
      conditionData.extend<int>( "rn" );
      conditionData["rn"].data<int>() = existing_run_number;
      query->setCondition( std::string(s_name_column) + " = :nm AND " + s_run_number_column + " = :rn", conditionData );

      coral::ICursor& cursor = query->execute();

      if( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        if(!row[s_duration_column].isNull()) {
          throw (rn::ExistingRun(ERS_HERE, m_db_name.c_str(), existing_run_number, "the run has been closed already"));
        }
        else {
          m_start_at = row[s_started_at_column].data<coral::TimeStamp>().total_nanoseconds();
          m_run_number = existing_run_number;
          return;
        }
      }

      throw (rn::ExistingRun(ERS_HERE, m_db_name.c_str(), existing_run_number, "the run does not exist"));
    }

      // set current time
    coral::TimeStamp now;
    m_start_at = now.total_nanoseconds();
    data[s_started_at_column].data<coral::TimeStamp>() = now;

      // for compatibility with old implementation
    data[s_start_at_column].data<std::string>() = ts2str(now);

      // explicitly ask duration value = NULL
    data[s_duration_column].setNull(true);
    data[s_lasted_column].setNull(true);

    data[s_release_column].data<std::string>() = TDAQ_RELEASE;

    { // set user name
      static System::User s_user(getuid());
      data[s_created_by_column].data<std::string>() = s_user.name_safe();
    }

    // set host name
    data[s_host_column].data<std::string>() = System::LocalHost::full_local_name();

    data[s_partition_name_column].data<std::string>() = m_partition_name;

    const char * tdaq_db_data_env = nullptr;

    if(m_config_version.empty())
      data[s_config_version_column].setNull(true);
    else
      {
        data[s_config_version_column].data<std::string>() = m_config_version;
        tdaq_db_data_env = getenv("TDAQ_DB_DATA");
      }

    if(tdaq_db_data_env && tdaq_db_data_env[0] != 0)
      {
        // strip off mapping dir if any
        std::string s(tdaq_db_data_env);
        if(s[0] == '/')
          {
            if (Oks::real_path(s, true))
              {
                if (!OksKernel::get_repository_mapping_dir().empty() && s.find(OksKernel::get_repository_mapping_dir()) == 0)
                  {
                    s.erase(0, OksKernel::get_repository_mapping_dir().size()+1);
                    ERS_DEBUG( 0, "strip TDAQ_DB_DATA=\"" << tdaq_db_data_env << "\" to repository name \"" << s << '\"');
                  }
              }
          }

        data[s_config_name_column].data<std::string>() = s;
      }
    else
      data[s_config_name_column].setNull(true);

    data[s_name_column].data<std::string>() = m_db_name;

      // get new version number
    {
      ERS_DEBUG(3, "Reading from database max run number for database \'" << m_db_name << "\'...");
      std::unique_ptr< coral::IQuery > query( m_table->newQuery() );
      query->setRowCacheSize( 1 );
      std::string max_rn_str = std::string("MAX(") + s_run_number_column + ')';
      query->addToOutputList( max_rn_str );
      query->defineOutputType( max_rn_str, "int");
      coral::AttributeList conditionData;
      conditionData.extend<std::string>( "n" );
      conditionData["n"].data<std::string>() = m_db_name;
      query->setCondition( std::string(s_name_column) + " = :n", conditionData );

      coral::ICursor& cursor = query->execute();

      if( cursor.next() && !cursor.currentRow().begin()->isNull() ) {
        m_run_number = cursor.currentRow().begin()->data<int>();
        ERS_DEBUG(3, "The maximum run number is " << m_run_number);
      }
      else {
        ERS_DEBUG(3, "There were no runs for such database name.");
      }
    }

    data[s_run_number_column].data<int>() = ++m_run_number;
    m_table->dataEditor().insertRow( data );

    ERS_DEBUG(3, "Committing...");
    m_session->transaction().commit();

    ERS_INFO("RunNumber service: create new run number \'" << m_run_number << '@' << m_db_name << '\'' );

    if (!m_config_version.empty())
      {
        TagRepository tag(m_run_number, m_partition_name, m_config_version);
        m_tag_thread = std::thread (tag);
      }

  }
  catch ( coral::Exception& e ) {
    throw (rn::CaughtException(ERS_HERE, "CORAL", e.what()));
  }
  catch ( std::exception& e ) {
    throw (rn::CaughtException(ERS_HERE, "Standard C++", e.what()));
  }
  catch ( ... ) {
    throw (rn::CaughtException(ERS_HERE, "Unknown", ""));
  }

}

void
RunNumber::clean_connection()
{
  ERS_DEBUG(3, "Clean session and connection...");

  if(m_session) {
    ERS_DEBUG(3, " * close session");
    delete m_session;
    m_session = 0;
  }

  if(m_connection_service) {
    ERS_DEBUG(3, " * disconnect");
    delete m_connection_service;
    m_connection_service = 0;
  }

  ERS_DEBUG(3, "Done");
}

void
RunNumber::close()
{
  if(m_run_number) {

    try {
      std::lock_guard<std::mutex> scoped_lock(s_run_number_mutex);

      m_session = get_session(m_db_connect, static_cast<int>(coral::Update), m_connection_service);
      ConnectionGuard __cg__(this);  // close connection on end of close()

      coral::ITable * m_table = &m_session->schema(m_db_owner).tableHandle( s_table_name );

      ERS_DEBUG(3, "Starting a new transaction...");
      m_session->transaction().start();

        // fill rest of the run  number information

      std::int64_t duration = coral::TimeStamp().total_nanoseconds() - m_start_at;

      if(duration < 0)
        {
          std::ostringstream text;
          text << "negative duration for run " << m_run_number << " started at " << m_start_at << " (ns since Epoch)";
          ers::error (rn::BadRunDuration(ERS_HERE, text.str().c_str()));
        }

      std::ostringstream s;
      s << s_duration_column << "=" << duration / 1000000000
        << ", " << s_lasted_column << "=" << duration;

      if(!m_comments.empty()) {
        std::replace(m_comments.begin(), m_comments.end(), '\'', '\"');
        s << ", " << s_comments_column << "=\'" << m_comments << '\'';
      }

      coral::AttributeList conditionData;
      conditionData.extend<std::string>( "nm" );
      conditionData["nm"].data<std::string>() = m_db_name;
      conditionData.extend<int>( "nb" );
      conditionData["nb"].data<int>() = m_run_number;

      m_table->dataEditor().updateRows(
        s.str(),
        std::string(s_name_column) + " = :nm AND " + s_run_number_column + " = :nb",
        conditionData
      );

      ERS_DEBUG(3, "Committing...");
      m_session->transaction().commit();
      
      m_run_number = 0;
    }

    catch ( coral::Exception& e ) {
      throw (rn::CaughtException(ERS_HERE, "CORAL", e.what()));
    }

    catch ( std::exception& e ) {
      throw (rn::CaughtException(ERS_HERE, "Standard C++", e.what()));
    }

    catch ( ... ) {
      throw (rn::CaughtException(ERS_HERE, "Unknown", ""));
    }
  }

  if (m_tag_thread.joinable())
    m_tag_thread.join();
}


coral::ISessionProxy *
RunNumber::get_session(const std::string& connect, int mode, coral::ConnectionService *& connection_service)
{
  coral::Context& context = coral::Context::instance();

  ERS_DEBUG(4, "Loading CORAL/Services/XMLAuthenticationService...");
  context.loadComponent( "CORAL/Services/XMLAuthenticationService" );

  ERS_DEBUG(4, "Loading CORAL/Services/RelationalService...");
  context.loadComponent( "CORAL/Services/RelationalService" );

  connection_service = new coral::ConnectionService();

  ERS_DEBUG(3, "Connecting and opening new session...");
  std::unique_ptr<coral::ISessionProxy> session( connection_service->connect( connect, static_cast<coral::AccessMode>(mode) ) );

  if( !session.get() ) {
    throw std::runtime_error( "Could not connect to " + connect );
  }

  ERS_DEBUG(3, "Starting a new transaction...");
  session->transaction().start(mode == coral::ReadOnly);

  return session.release();
}


} /* close namespace 'tdaq' */
