// select NAME, RUNNUMBER, CREATEDBY,  STARTAT, STOPAT, CONFIGSCHEMA, CONFIGDATA, COMMENTS from RunNumber;

#include <stdlib.h>
#include <iostream>
#include <algorithm>

#include <boost/program_options.hpp>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/AttributeSpecification.h>
#include <CoralBase/Exception.h>
#include <CoralBase/TimeStamp.h>
#include "CoralKernel/Context.h"

#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ITableDescription.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/SchemaException.h>

#include <ers/ers.h>

#include <rn/rn.h>
#include <oks/tz.h>

ERS_DECLARE_ISSUE(
  rn_ls,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

  // string to duration

static int
s2du(const std::string &in, const char *name)
{
  int du = 0;

  try
    {
      const auto d = boost::posix_time::duration_from_string(in);
      du = d.seconds() + d.minutes() * 60 + d.hours() * 3600;
      ERS_DEBUG(1, "Convert  \'" << in << "\' -> = \'" << du << '\'');
    }
  catch (const std::exception& ex)
    {
      throw std::runtime_error(std::string("cannot parse ") + name + " parameter = \'" + in + "\'): " + ex.what());
    }

  if (du < 0)
    throw std::runtime_error(std::string("negative run duration = \'") + in + "\' is not allowed");

  return du;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct TableRow {

  char m_fill;
  char m_separator;
  std::string m_items[11];

  TableRow(const char fill, const char separator) : m_fill(fill), m_separator(separator) { ; }

  TableRow(const char fill, const char separator, boost::local_time::time_zone_ptr tz_ptr, int version_length,
           const std::string& db_name, int rn, const std::string& start_at, std::int64_t nanosecond,
           int duration, std::int64_t lasted, const std::string& release, const std::string& user, const std::string& host, const std::string& partition,
	   int sv, int dv, const std::string& cv, const std::string& cn, const std::string& comments) : m_fill(fill), m_separator(separator) {

    m_items[0] = db_name;
    { std::ostringstream s; s << rn; m_items[1] = s.str(); }
    { // show date-time in UTC or time zone provided by user
      boost::posix_time::ptime t(boost::posix_time::from_iso_string(start_at));
      if(tz_ptr) {
        m_items[2] = boost::local_time::local_date_time(t, tz_ptr).to_string();
      }
      else {
        m_items[2] = boost::posix_time::to_simple_string(t);
      }

      if(nanosecond >= 0)
        {
          std::ostringstream s;
          s << '.';
          s.fill('0'); s.width(3); s << nanosecond / 1000000;

            if (tz_ptr)
              {
                auto idx = m_items[2].find(' ');
                if (idx != std::string::npos)
                  idx = m_items[2].find(' ', idx + 1);
                if (idx != std::string::npos)
                  m_items[2].insert(idx, s.str());
                else
                  m_items[2].append(s.str());
              }
            else
              m_items[2].append(s.str());
        }
    }
    if(duration >= 0) {
      long h = duration / 3600; duration -= (h * 3600);
      long m = duration / 60; duration -= (m * 60);
      std::ostringstream s; s << h << ':'; s.fill('0'); s.width(2); s << m; s << ':'; s.width(2); s << duration;
      if(lasted > 0) {
        s << '.';
        s.fill('0'); s.width(3); s << (lasted%1000000000) / 1000000;
      }
      m_items[3] = s.str();
    }
    m_items[4] = release;
    m_items[5] = user;
    m_items[6] = host;
    m_items[7] = partition;
    if(sv > 0 && dv > 0) { std::ostringstream s; s << sv << '.' << dv; m_items[8] = s.str(); }
    else if(!cv.empty()) {
      if(version_length != -1) {
        if(cv.find("hash:") == 0)
          m_items[8] = cv.substr(5, (int)cv.length() >= (version_length + 5) ? version_length : std::string::npos);
      }
      else
        m_items[8] = cv;
    }
    m_items[9] = cn;
    m_items[10] = comments;
  }

  TableRow(const char fill, const char separator, const std::string& s1, const std::string& s2,
           const std::string& s3, const std::string& s4, const std::string& s5, const std::string& s6,
	   const std::string& s7, const std::string& s8, const std::string& s9, const std::string& s10,
	   const std::string& s11) : m_fill(fill), m_separator(separator) {
    m_items[0] = s1;
    m_items[1] = s2;
    m_items[2] = s3;
    m_items[3] = s4;
    m_items[4] = s5;
    m_items[5] = s6;
    m_items[6] = s7;
    m_items[7] = s8;
    m_items[8] = s9;
    m_items[9] = s10;
    m_items[10] = s11;
  }

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static bool
has_a_pattern(const std::string& s)
{
  return(
    ( s.find("%") != std::string::npos ) ||
    ( s.find("_") != std::string::npos ) ||
    ( s.find("[") != std::string::npos && s.find("]") != std::string::npos) ? true : false
  );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char * argv[])
{
  const long c_min_run_number = 1;
  const long c_max_run_number = 0x7FFFFFFF; //(1<<31) - 1;

  boost::program_options::variables_map vm;

  bool list_db;
  std::string db_name;
  std::string connect_string;
  std::string working_schema;
  std::string tz;
  int version_length = -1;
  boost::local_time::time_zone_ptr tz_ptr;
  std::string started_since;
  std::string started_till;
  int min_duration = -1;
  int max_duration = -1;
  long min_rn = c_min_run_number;
  long max_rn = c_max_run_number;
  std::string release;
  std::string user;
  std::string host;
  std::string partition;
  std::string name;
  std::string sorted_by;

  try
    {
      std::string tmp_started_since;
      std::string tmp_started_till;
      std::string tmp_min_duration;
      std::string tmp_max_duration;

      boost::program_options::options_description desc("The utility reports information about ATLAS run numbers.\n"
        "Use \"-z \'Europe/Zurich\'\" for CERN timezone, the timestamps are in UTC by default.\n"
        "The timestamps to be provided in ISO 8601 format: \"YYYY-MM-DD HH:MM:SS\".\n"
        "The durations to be provided in format: \"HH:MM:SS\".\n"
        "The allowed wildcard characters in patterns below are:\n"
        "  % (i.e. percent symbol)     - any string of zero or more characters;\n"
        "  _ (i.e. underscore symbol)  - any single character.\n\n"
        "The command line options are");

      desc.add_options()
        ( "connect-string,c", boost::program_options::value<std::string>(&connect_string)->required(), "database connection string" )
        ( "working-schema,w", boost::program_options::value<std::string>(&working_schema)->required(), "name of working schema" )
        ( "list-databases,l", "list database names holding run numbers" )
        ( "database,d", boost::program_options::value<std::string>(&db_name), "name of the database holding run numbers" )
        ( "release,r", boost::program_options::value<std::string>(&release), "show run numbers for release names satisfying pattern (see syntax description)" )
        ( "user,e", boost::program_options::value<std::string>(&user), "show run numbers for user names satisfying pattern (see syntax description)" )
        ( "host,o", boost::program_options::value<std::string>(&host), "show run numbers for hostnames satisfying pattern (see syntax description)" )
        ( "partition,p", boost::program_options::value<std::string>(&partition), "show run numbers for partition names satisfying pattern (see syntax description)" )
        ( "config-name,a", boost::program_options::value<std::string>(&name), "show run numbers for config names satisfying pattern (see syntax description)" )
        ( "started-since,s", boost::program_options::value<std::string>(&tmp_started_since), "show run numbers started since given moment" )
        ( "started-till,t", boost::program_options::value<std::string>(&tmp_started_till), "show run numbers started before given moment" )
        ( "minimal-duration,f", boost::program_options::value<std::string>(&tmp_min_duration), "show runs last longer given duration" )
        ( "maximal-duration,i", boost::program_options::value<std::string>(&tmp_max_duration), "show runs last shorter given duration" )
        ( "minimal-run-number,n", boost::program_options::value<long>(&min_rn)->default_value(c_min_run_number), "show run numbers greater or equal to given one" )
        ( "maximal-run-number,m", boost::program_options::value<long>(&max_rn)->default_value(c_max_run_number), "show run numbers less or equal to given one" )
        ( "time-zone,z", boost::program_options::value<std::string>(&tz), "time zone name (run with \"-z list-regions\" to see all supported time zones)" )
        ( "sorted-by,q", boost::program_options::value<std::string>(&sorted_by), "sort output by several columns; the parameters may contain the following items (where first symbol is for ascending and second for descending order):\n"
          "  n | N - sort by run number;\n"
          "  m | M - sort by database name;\n"
          "  d | D - sort by duration;\n"
          "  p | P - sort by partition names;\n"
          "  r | R - sort by release names;\n"
          "  u | U - sort by user names;\n"
          "  h | H - sort by hostnames;\n" )
        ( "short-sha,x", boost::program_options::value<int>(&version_length), "shortens the number of characters in the config git sha output (minimum 4)" )
        ( "help,h", "Print help message");

      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.find("help") != vm.end())
        {
          std::cout << desc << std::endl;
          return 0;
        }

      list_db = (vm.find("list-databases") != vm.end());

      boost::program_options::notify(vm);

      if (!tz.empty())
        {
          oks::tz::DB tz_db;

          if (tz == "list-regions")
            {
              for (auto& i : tz_db.get_regions())
                std::cout << i << std::endl;

              return 0;
            }

          tz_ptr = tz_db.get_tz_ptr(tz);
        }

       if (!tmp_started_since.empty())
        started_since = boost::posix_time::to_iso_string(oks::tz::str_2_posix_time(tmp_started_since, tz_ptr, "started-since"));

      if (!tmp_started_till.empty())
        started_till = boost::posix_time::to_iso_string(oks::tz::str_2_posix_time(tmp_started_till, tz_ptr, "started-till"));

      if (!tmp_min_duration.empty())
        min_duration = s2du(tmp_min_duration, "minimal-duration");

      if (!tmp_max_duration.empty())
        max_duration = s2du(tmp_max_duration, "maximal-duration");

      if (min_rn < c_min_run_number)
        throw std::runtime_error("minimal-run-number must be greater 0");

      if (max_rn < min_rn)
        throw std::runtime_error("maximal-run-number must be greater or equal minimal-run-number");

      if ((max_duration != -1) && (min_duration != -1) && (max_duration < min_duration))
        throw std::runtime_error("maximal-duration must be greater or equal minimal-duration");

      if (version_length != -1 && version_length < 4)
        throw std::runtime_error("minimal git sha short version length is 4");
    }
  catch(const std::exception& ex)
    {
      ers::fatal( rn_ls::BadCommandLine( ERS_HERE, ex.what() ) );
      return 1;
    }


  try {
    coral::ConnectionService * connection = 0;
    std::unique_ptr<coral::ISessionProxy> session (tdaq::RunNumber::get_session(connect_string, static_cast<int>(coral::ReadOnly), connection));

    coral::ITable& table = session->schema(working_schema).tableHandle( tdaq::RunNumber::s_table_name );

    if(list_db) {
      std::unique_ptr< coral::IQuery > query( table.newQuery() );
      query->setRowCacheSize( 10 );
      query->addToOutputList( std::string("DISTINCT ") + tdaq::RunNumber::s_name_column );
      coral::ICursor& cursor = query->execute();

      std::set<std::string> names;

      while(cursor.next()) {
        names.insert(cursor.currentRow().begin()->data<std::string>());
      }
      
      std::cout << "Found " << names.size() << " run number databases:\n";
      
      for(std::set<std::string>::const_iterator i = names.begin(); i != names.end(); ++i) {
        std::cout << " - \'" << *i << '\'' << std::endl;
      }
    }
    else {
      std::unique_ptr< coral::IQuery > query( table.newQuery() );
      query->setRowCacheSize( 10 );

      coral::AttributeList conditions;
      std::set<std::string> conditions_set;

      if(!db_name.empty()) {
        conditions.extend<std::string>( "nm" );
        conditions["nm"].data<std::string>() = db_name;
        conditions_set.insert(std::string(tdaq::RunNumber::s_name_column) + " = :nm");
      }

      if(!partition.empty()) {
        conditions.extend<std::string>( "prt" );
        conditions["prt"].data<std::string>() = partition;
	conditions_set.insert(std::string(tdaq::RunNumber::s_partition_name_column) + (has_a_pattern(partition) ? " like" : " =") + " :prt");
      }

      if(!name.empty()) {
        conditions.extend<std::string>( "cn" );
        conditions["cn"].data<std::string>() = name;
        conditions_set.insert(std::string(tdaq::RunNumber::s_config_name_column) + (has_a_pattern(name) ? " like" : " =") + " :cn");
      }

      if(!release.empty()) {
        conditions.extend<std::string>( "rl" );
        conditions["rl"].data<std::string>() = release;
	conditions_set.insert(std::string(tdaq::RunNumber::s_release_column) + (has_a_pattern(release) ? " like" : " =") + " :rl");
      }

      if(!user.empty()) {
        conditions.extend<std::string>( "usr" );
        conditions["usr"].data<std::string>() = user;
        conditions_set.insert(std::string(tdaq::RunNumber::s_created_by_column) + (has_a_pattern(user) ? " like" : " =") + " :usr");
      }

      if(!host.empty()) {
        conditions.extend<std::string>( "hst" );
        conditions["hst"].data<std::string>() = host;
        conditions_set.insert(std::string(tdaq::RunNumber::s_host_column) + (has_a_pattern(host) ? " like" : " =") + " :hst");
      }

      if(!started_since.empty()) {
        conditions.extend<std::string>( "ss" );
        conditions["ss"].data<std::string>() = started_since;
        conditions_set.insert(std::string(tdaq::RunNumber::s_start_at_column) + " >= :ss");
      }

      if(!started_till.empty()) {
        conditions.extend<std::string>( "st" );
        conditions["st"].data<std::string>() = started_till;
        conditions_set.insert(std::string(tdaq::RunNumber::s_start_at_column) + " <= :st");
      }

      if(min_duration != -1) {
        conditions.extend<int>( "min_d" );
        conditions["min_d"].data<int>() = min_duration;
        conditions_set.insert(std::string(tdaq::RunNumber::s_duration_column) + " >= :min_d");
      }

      if(max_duration != -1) {
        conditions.extend<int>( "max_d" );
        conditions["max_d"].data<int>() = max_duration;
        if(max_duration == 0) {
          conditions_set.insert(std::string("(") + tdaq::RunNumber::s_duration_column + " <= :max_d OR " + tdaq::RunNumber::s_duration_column + " is NULL)");
        }
        else { 
          conditions_set.insert(std::string(tdaq::RunNumber::s_duration_column) + " <= :max_d");
        }
      }

      if(min_rn != c_min_run_number) {
        conditions.extend<int>( "min_rn" );
        conditions["min_rn"].data<int>() = static_cast<int>(min_rn);
        conditions_set.insert(std::string(tdaq::RunNumber::s_run_number_column) + " >= :min_rn");
      }

      if(max_rn != c_max_run_number) {
        conditions.extend<int>( "max_rn" );
        conditions["max_rn"].data<int>() = static_cast<int>(max_rn);
        conditions_set.insert(std::string(tdaq::RunNumber::s_run_number_column) + " <= :max_rn");
      }
    
      std::string conditions_str;
    
      for(std::set<std::string>::const_iterator i = conditions_set.begin(); i != conditions_set.end(); ++i) {
        if(i != conditions_set.begin()) conditions_str += " AND ";
        conditions_str += *i;
      }

      if(!sorted_by.empty()) {
        for(const char * p = sorted_by.c_str(); *p != 0; ++p) {
          switch (*p) {
            case 'n': query->addToOrderList(tdaq::RunNumber::s_run_number_column); break;
            case 'N': query->addToOrderList(std::string(tdaq::RunNumber::s_run_number_column) + " DESC"); break;
            case 'm': query->addToOrderList(tdaq::RunNumber::s_name_column); break;
            case 'M': query->addToOrderList(std::string(tdaq::RunNumber::s_name_column) + " DESC"); break;
            case 'd': query->addToOrderList(tdaq::RunNumber::s_duration_column); break;
            case 'D': query->addToOrderList(std::string(tdaq::RunNumber::s_duration_column) + " DESC"); break;
            case 'p': query->addToOrderList(tdaq::RunNumber::s_partition_name_column); break;
            case 'P': query->addToOrderList(std::string(tdaq::RunNumber::s_partition_name_column) + " DESC"); break;
            case 'u': query->addToOrderList(tdaq::RunNumber::s_created_by_column); break;
            case 'U': query->addToOrderList(std::string(tdaq::RunNumber::s_created_by_column) + " DESC"); break;
            case 'h': query->addToOrderList(tdaq::RunNumber::s_host_column); break;
            case 'H': query->addToOrderList(std::string(tdaq::RunNumber::s_host_column) + " DESC"); break;
            case 'r': query->addToOrderList(tdaq::RunNumber::s_release_column); break;
            case 'R': query->addToOrderList(std::string(tdaq::RunNumber::s_release_column) + " DESC"); break;
           default:
              std::stringstream text;
              text << "ERROR: wrong value \'" << *p << "\' in the command line parameter: \"--sorted-by\" or \"-t\"";
              throw std::runtime_error( text.str().c_str() );
          }
        }
      }
      else {
        query->addToOrderList(tdaq::RunNumber::s_name_column);
        query->addToOrderList(std::string(tdaq::RunNumber::s_run_number_column) + " DESC");
      }

      query->setCondition( conditions_str, conditions );

      coral::ICursor& c = query->execute();

      std::vector<TableRow> rows;

        // fill the table

      rows.push_back(TableRow('=', '='));
      rows.push_back(TableRow(' ', '|', "Name","Num",(tz_ptr ? "Start At (local time)" : "Start At (UTC)"),"Duration","Release","User","Host","Partition","Version","Config Name","Comment"));
      rows.push_back(TableRow('=', '='));

      while(c.next()) {
        const coral::AttributeList& row = c.currentRow();
      
        int duration = -1;
        if(!row[tdaq::RunNumber::s_duration_column].isNull()) duration = row[tdaq::RunNumber::s_duration_column].data<int>();

        std::int64_t lasted = -1;
        if(!row[tdaq::RunNumber::s_lasted_column].isNull()) lasted = row[tdaq::RunNumber::s_lasted_column].data<long long>();

        std::int64_t started_at = -1;
        if(!row[tdaq::RunNumber::s_started_at_column].isNull()) started_at = row[tdaq::RunNumber::s_started_at_column].data<coral::TimeStamp>().nanosecond();

        std::string comments;
        if(!row[tdaq::RunNumber::s_comments_column].isNull()) comments = row[tdaq::RunNumber::s_comments_column].data<std::string>();

        int sv = -1;
        if(!row[tdaq::RunNumber::s_config_schema_column].isNull()) sv = row[tdaq::RunNumber::s_config_schema_column].data<int>();

        int dv = -1;
        if(!row[tdaq::RunNumber::s_config_data_column].isNull()) dv = row[tdaq::RunNumber::s_config_data_column].data<int>();

        std::string cv;
        if(!row[tdaq::RunNumber::s_config_version_column].isNull()) cv = row[tdaq::RunNumber::s_config_version_column].data<std::string>();

        std::string cn;
        if(!row[tdaq::RunNumber::s_config_name_column].isNull()) cn = row[tdaq::RunNumber::s_config_name_column].data<std::string>();

        rows.push_back(
          TableRow(' ', '|', tz_ptr, version_length,
            row[tdaq::RunNumber::s_name_column].data<std::string>(),
	    row[tdaq::RunNumber::s_run_number_column].data<int>(),
            row[tdaq::RunNumber::s_start_at_column].data<std::string>(),
            started_at,
	    duration,
	    lasted,
            row[tdaq::RunNumber::s_release_column].data<std::string>(),
	    row[tdaq::RunNumber::s_created_by_column].data<std::string>(),
	    row[tdaq::RunNumber::s_host_column].data<std::string>(),
	    row[tdaq::RunNumber::s_partition_name_column].data<std::string>(),
	    sv, dv, cv, cn,
  	    comments
          )
        );

      }

      rows.push_back(TableRow('=', '='));

        // detect columns widths

      unsigned short cw[] = {1,1,1,1,1,1,1,1,1,1,1};
      {
        for(std::vector<TableRow>::const_iterator i = rows.begin(); i != rows.end(); ++i) {
          for(unsigned short j = 0; j < sizeof(cw)/sizeof(unsigned short); ++j) {
            unsigned short len = (*i).m_items[j].size();
            if(len > cw[j]) cw[j]=len;
          }
        }
      }


        // print table

      {
        bool align_left[] = {false, false, false, false, false, false, false, false, false, false, true};

        for(std::vector<TableRow>::const_iterator i = rows.begin(); i != rows.end(); ++i) {
          std::cout.fill((*i).m_fill);
          for(unsigned short j = 0; j < sizeof(cw)/sizeof(unsigned short); ++j) {
            std::cout << (*i).m_separator << (*i).m_fill;
            std::cout.setf((align_left[j] ? std::ios::left : std::ios::right), std::ios::adjustfield);
            std::cout.width(cw[j]);
            std::cout << (*i).m_items[j] << (*i).m_fill;
          }
          std::cout << (*i).m_separator << std::endl;
      }
      }

    }

    session->transaction().commit();
    //connection->disconnect();
  }

  catch ( coral::Exception& e ) {
    std::cerr << "CORAL exception: " << e.what() << std::endl;
    return 11;
  }

  catch ( std::exception& e ) {
    std::cerr << "Standard C++ exception : " << e.what() << std::endl;
    return 12;
  }

  catch ( ... ) {
    std::cerr << "Exception caught (...)" << std::endl;
    return 13;
  }

  return 0;
}
