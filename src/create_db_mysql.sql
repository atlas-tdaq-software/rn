create table RUNNUMBER (
  NAME varchar(16) not null,
  RUNNUMBER integer(10) not null,
  STARTAT varchar(24) not null,
  DURATION integer(10),
  CREATEDBY varchar(16) not null,
  HOST varchar(256) not null,
  PARTITIONNAME varchar(256) not null,
  CONFIGSCHEMA integer(10),
  CONFIGDATA integer(10),
  COMMENTS varchar(2000)
);
