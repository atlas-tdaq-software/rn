rem set termout off

drop table RunNumber;

create table RunNumber (
  Name           varchar2(16) not null,
  RunNumber      number(10) not null,
  StartAt        varchar2(24) not null,
  Duration       number(10),
  CreatedBy      varchar2(16) not null,
  Host           varchar2(256) not null,
  PartitionName  varchar2(256) not null,
  ConfigSchema   number(10),
  ConfigData     number(10),
  Comments       varchar2(2000),
  constraint     RunNumber_PK primary key (Name, RunNumber),
  constraint     RunNumber_Duration_CN check ((Duration is null) or (Duration >= 0))
);

rem set termout on
