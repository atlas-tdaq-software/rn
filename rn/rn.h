/**	
 *	\file rn/rn.h
 *
 *	This file is part of the rn (RunNumber) package.
 *	Author: <Igor.Soloviev@cern.ch>
 *
 *	This file contains the declarations for the RunNumber class.
 */

#ifndef TDAQ_RN_H
#define TDAQ_RN_H

#include <cstdint>
#include <string>
#include <thread>

#include <ers/ers.h>

namespace coral {
  class ISessionProxy;
  class ConnectionService;
}

namespace tdaq {

   /**
     *  \class rn::Exception
     *  This is a base class for all daq::rn exceptions thrown by Run Number package (rn).
     */

  ERS_DECLARE_ISSUE( rn, Exception, , )


    /**
     * \brief Provides interface to get new run number and write associated data.
     *
     *  To get new run number it is necessary to create an object of the RunNumber class.
     *  In case of problems the methods of the RunNumber class may throw exceptions
     *  derived from the rn::Exception base class.
     *
     *  It's constructor talks with the relational database and creates new run number record, that includes:
     *  - name of the run number database,
     *  - the run number,
     *  - run-start timestamp,
     *  - partition name,
     *  - user name,
     *  - host name.
     *  In case of a problem the constructor throws ers exception.
     *  
     *  When the object is destructed, it talks with database again and updates second part of the run number record:
     *  - stop timestamp,
     *  - archived configuration's schema and data versions,
     *  - optional user's comments.
     *  In case of a problem the destructor also throws ers exception.
     *
     *  To get run number one can use method run_number() invoked on the created object. If the method returns 0,
     *  this indicates a problem.
     *
     *  It is possible to store run comments using set_comments(const std::string&) method.
     */


  class RunNumber {

    friend class ConnectionGuard;

    public:

        /**
         *  \brief Create new run number.
         *
         *  The method gets new run number and stores first part of run information.
         *  The method's parameters are:
         *  \param connect       the connect string
         *  \param partition     the partition name
         *  \param existing_rn   an existing run number to close open run
         *
         *  The connect parameter has the following format: "db-name:db-owner:db-connect-string", where:
         *  - the db-name is a string literal (max 16 characters) to define a database name for different purposes, e.g. 'nightly', 'Lab32', 'point-1', etc.; the run number will be unique for each db-name.
         *  - the db-owner is the name of RunNumber table owner.
         *  - the db-connect-string is CORAL specific database connect string.
         *  The db-owner and the db-connect-string are site specific.
         *  If the connect parameter is empty, it's value is taken from the TDAQ_RUN_NUMBER_CONNECT environment variable.
         *  If the connect parameter is empty and the environment variable is not defined or is empty, then it is considered as an error.
         *
         *  The partition parameter defines name of the partition. If partition parameter is empty, it is read from TDAQ_PARTITION environment variable.
         *  If the partition parameter is empty and the environment variable is not defined or is empty, then it is considered as an error.
         *
         *  The optional existing_rn parameter can be used to store second part of run information in case if it has not been done already.
         *  If the existing_rn parameter is set to non-zero, corresponding run number record must exist and not closed.
         */


      RunNumber(const std::string& connect, const std::string& partition, unsigned long existing_rn = 0);


        /** Destroy RunNumber object. Call close() method before destructor to catch possible exceptions. **/

      ~RunNumber() { close(); }


        /**
         *   Closes the run and stores final part of run information.
         *   \throw rn::Exception in case of problems.
         */

      void close();


        /** The method return number of current run. **/

      unsigned long get_number() const noexcept { return m_run_number; }


        /** The method allows to provide comments for given run. **/

      void set_comments(const std::string& comments) noexcept { m_comments = comments; }


        /** Internal method to be shared by library and the rn_ls application. **/

      static coral::ISessionProxy * get_session(const std::string& connect, int mode, coral::ConnectionService *& connection);


    private:

      unsigned long m_run_number;
      std::string m_db_name;
      std::string m_comments;
      std::string m_partition_name;
      std::int64_t m_start_at;
      std::string m_config_version;

      coral::ISessionProxy * m_session;
      coral::ConnectionService * m_connection_service;

      std::string m_db_owner;
      std::string m_db_connect;

      std::thread m_tag_thread;

      void clean_connection();


    public:


        /** Internal variable defining relational table name to be shared by library and the ls application. **/
      static const char * s_table_name;

        /** Internal variable defining name of the 'Name' column to be shared by library and the ls application. **/
      static const char * s_name_column;

        /** Internal variable defining name of the 'RunNumber' column to be shared by library and the ls application. **/
      static const char * s_run_number_column;

        /** Internal variable defining name of the 'StartAt' column to be shared by library and the ls application. **/
      static const char * s_start_at_column;

       /** Internal variable defining name of the 'StartedAt' column (nanoseconds precision) to be shared by library and the ls application. **/
      static const char * s_started_at_column;

        /** Internal variable defining name of the 'Duration' column to be shared by library and the ls application. **/
      static const char * s_duration_column;

        /** Internal variable defining name of the 'Lasted' column (duration in nanoseconds) to be shared by library and the ls application. **/
      static const char * s_lasted_column;

        /** Internal variable defining name of the 'Release' column to be shared by library and the ls application. **/
      static const char * s_release_column;

        /** Internal variable defining name of the 'CreatedBy' column to be shared by library and the ls application. **/
      static const char * s_created_by_column;

        /** Internal variable defining name of the 'Host' column to be shared by library and the ls application. **/
      static const char * s_host_column;

        /** Internal variable defining name of the 'PartitionName' column to be shared by library and the ls application. **/
      static const char * s_partition_name_column;

        /** Internal variable defining name of the 'ConfigSchema' column to be shared by library and the ls application. **/
      static const char * s_config_schema_column;

        /** Internal variable defining name of the 'ConfigData' column to be shared by library and the ls application. **/
      static const char * s_config_data_column;

        /** Internal variable defining name of the 'ConfigSchema' column to be shared by library and the ls application. **/
      static const char * s_config_version_column;

        /** Internal variable defining name of the 'ConfigName' column to be shared by library and the ls application. **/
      static const char * s_config_name_column;

        /** Internal variable defining name of the 'Comments' column to be shared by library and the ls application. **/
      static const char * s_comments_column;

  };


    /**
     *  \class rn::NoParameter
     *  Cannot get parameter's value.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    NoParameter,
    rn::Exception,
    "The \'" << parameter_name << "\' parameter is not defined, check parameters of the constructor or value of the \'" << variable_name << "\' variable",
    ,
    ((const char*)parameter_name)
    ((const char*)variable_name)
  )


    /**
     *  \class rn::BadConnectParameter
     *  Failed to parse connect string.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    BadConnectParameter,
    rn::Exception,
    "Cannot find \'" << parameter_name << "\' in the \'" << v << "\' variable (format: \"db-name:db-owner:db-connect-string\")",
    ,
    ((const char*)parameter_name)
    ((const char*)v)
  )


    /**
     *  \class rn::CaughtException
     *  Caught CORAL, SEAL or relational database run-time problem.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    CaughtException,
    rn::Exception,
    "Caught " << what << " exception: \'" << text << "\'",
    ,
    ((const char *)what)
    ((const char *)text)
  )


    /**
     *  \class rn::ExistingRun
     *  There is a problem with exiting run.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    ExistingRun,
    rn::Exception,
    "Cannot create object for run " << number << '@' << name << ": " << reason,
    ,
    ((const char *)name)
    ((unsigned long)number)
    ((const char *)reason)
  )


    /**
     *  \class rn::BadRunDuration
     *  There is a problem with calculation of run duration.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    BadRunDuration,
    rn::Exception,
    "Cannot calculate run duration: \'" << reason << '\'',
    ,
    ((const char *)reason)
  )


    /**
     *  \class rn::TagConfigRepositoryFailure
     *  There is a problem with tagging of the config repository.
     */

  ERS_DECLARE_ISSUE_BASE(
    rn,
    TagConfigRepositoryFailure,
    rn::Exception,
    "Cannot tag config repository:\n\tcaused by: " << reason,
    ,
    ((const char *)reason)
  )

}


#endif
