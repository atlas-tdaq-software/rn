#!/usr/bin/perl

  use CGI qw/:standard :html3/;

#  $tdaq_release='tdaq-04-00-01';
#  $tdaq_release='tdaq-07-01-00';
  $tdaq_release='tdaq-09-02-01';

#  @DB = (
#    { name => "TDAQ Development", connect_as => "oracle://devdb10/tdaq_dev_backup", db_owner => "onlcool", oks_connect_as => "oracle://devdb10/tdaq_dev_backup", oks_db_owner => "onlcool" },
#    { name => "Point-1 (offline) - outside Point-1", connect_as => "oracle://atlr/rn_r", db_owner => "atlas_run_number", oks_connect_as => "oracle://atlas_oks/r", oks_db_owner => "atlas_oks_archive" },
#    { name => "Point-1 (online) - inside Point-1", connect_as => "oracle://atonr/rn_r", db_owner => "atlas_run_number", oks_connect_as => "oracle://atonr_oks/r", oks_db_owner => "atlas_oks_archive" },
#  );

  @DB = (
    { name => "Point-1 (offline)", connect_as => "oracle://atlr/rn_r", db_owner => "atlas_run_number", oks_connect_as => "oracle://atlas_oks/r", oks_db_owner => "atlas_oks_archive", git_base_url => "https://gitlab.cern.ch/atlas-tdaq-oks/p1" },
    { name => "TDAQ Development", connect_as => "oracle://int8r/tdaq_dev_backup_r", db_owner => "atlas_oks_archive", oks_connect_as => "oracle://int8r/tdaq_dev_backup_r", oks_db_owner => "atlas_oks_archive", git_base_url => "https://gitlab.cern.ch/atlas-tdaq-oks/tbed" },
  );

  @INTERVALS = ( '*','1 hour','2 hours','6 hours','12 hours',
                 '1 day','2 days','1 week','2 weeks',
		 '1 month','2 months','6 months','1 year','2 years');

  @DURATIONS = (
    { name => "*",          value => "*"        },
    { name => "5 seconds",  value => "00:00:05" },
    { name => "20 seconds", value => "00:00:20" },
    { name => "1 minute",   value => "00:01:00" },
    { name => "2 minutes",  value => "00:02:00" },
    { name => "5 minutes",  value => "00:05:00" },
    { name => "10 minutes", value => "00:10:00" },
    { name => "30 minutes", value => "00:30:00" },
    { name => "1 hour",     value => "01:00:00" },
    { name => "2 hours",    value => "02:00:00" },
    { name => "5 hours",    value => "05:00:00" },
    { name => "12 hours",   value => "12:00:00" },
    { name => "1 day",      value => "24:00:00" },
  );


  @DBNAMES = ('');
  @NAMES = ('*');

  for $i ( 0 .. $#DB ) {
    $v=$DB[$i]{"name"};
    push @DBNAMES, $v;
  }

  $db_name='';
  $connect_as='';
  $db_owner='';
  $oks_connect_as='';
  $oks_db_owner='';
  $git_base_url='';


  @QUERY_PARAMS = ('dbh','names','started_since','started_until','min_dur','max_dur','min_rn','max_rn','host','user','partition','release','file');


   # read query parameters passed via param()

  %parameters = ();

  if(param('dbh')) {
    foreach (@QUERY_PARAMS) {
      $parameters{$_} = param($_) || '';
    }
  }

   # set last query cookies

  @QUERIES = ();

  foreach $q ('q0','q1','q2','q3','q4','q5','q6','q7','q8','q9') {
    $rec={};
    $rec->{'name'} = "$q";
    $rec->{'value'} = { cookie("RunNumber:$q") };
    push @QUERIES, $rec;
  }

  $qx=-1;

  if(param('btn3')) {
    $t=param('time_int');
    if ("$t" ne "") {
      $t =~ s/^\s*(.*?)\s*$/$1/;
      if("$t" ne "*") { chomp($x = `date -u '+%F %T' --date="$t ago"`); }
    }
    $parameters{'started_since'}=$x;

    $x=param('mnd');
    if("$x" ne "") {
      $x =~ s/^\s*(.*?)\s*$/$1/;
      if("$x" ne "*") {
        for $i ( 0 .. $#DURATIONS ) {
          if ($x eq "$DURATIONS[$i]{'name'}") { $x=$DURATIONS[$i]{"value"}; last; }
        }
      }
      else { $x = ''; }
    }
    $parameters{'min_dur'}=$x;

    $x=param('mxd');
    if("$x" ne "") {
      $x =~ s/^\s*(.*?)\s*$/$1/;
      if("$x" ne "*") {
        for $i ( 0 .. $#DURATIONS ) {
          if ($x eq "$DURATIONS[$i]{'name'}") { $x=$DURATIONS[$i]{"value"}; last; }
        }
      }
      else { $x = ''; }
    }
    $parameters{'max_dur'}=$x;
  }
  elsif(param('btn0')) {
    $i=param('selected_request');
    foreach $x (@QUERY_PARAMS) {
      $parameters{$x} = $QUERIES[$i]{'value'}{$x};
    }
  }

  if($parameters{'dbh'}) {
    for $i ( 0 .. $#QUERIES ) {
      $r=1;
      foreach $x (@QUERY_PARAMS) {
        if("$parameters{$x}" ne "$QUERIES[$i]{'value'}{$x}") { $r=0; last; }
      }
      if($r == 1) { $qx=$i; last; }
    }

      # add new query
    if($qx == -1) {
      foreach $x (@QUERY_PARAMS) {
        for ($i = $#QUERIES; $i > 0; $i-- ) {
          $QUERIES[$i]{'value'}{"$x"} = $QUERIES[$i-1]{'value'}{"$x"};
        }
        $QUERIES[0]{'value'}{"$x"} = $parameters{"$x"};
      }
    }
      # swap/shift queries
    elsif($qx != 0) {
      foreach $x (@QUERY_PARAMS) {
        $y = $QUERIES[$qx]{'value'}{"$x"};
        for ($i = $qx; $i > 0; $i-- ) {
          $QUERIES[$i]{'value'}{"$x"} = $QUERIES[$i-1]{'value'}{"$x"};
        }
        $QUERIES[0]{'value'}{"$x"} = $y;
      }
    }
  }

   # set shown table columns cookies

  %table_details_c = cookie('RunNumber:table_details');

  @EXTRA_TABLE_COLUMNS = ('release','user','host','config','file','comments');

   # if the check boxes are set, overwrite cookie values
  if(param('table_details')) {
    foreach (@EXTRA_TABLE_COLUMNS) {
      $table_details_c{$_} = 'no';
    }

    @tbl_d=param('table_details');

    for $i ( 0 .. $#tbl_d ) {
      $d=$tbl_d[$i];
      $table_details_c{$d}='yes';
    }
  }

  foreach (@EXTRA_TABLE_COLUMNS) {
    $table_details_c{$_} = $table_details_c{$_} || 'no';
  }

  $tz_name = param('tmz') || cookie('RunNumber:time-zone') || 'UTC';

  push @COOKIES, cookie(-name=>'RunNumber:time-zone',-value=>"$tz_name",-expires=>'+180d');
  push @COOKIES, cookie(-name=>'RunNumber:table_details', -value=>\%table_details_c, -expires=>'+180d');

  for $i ( 0 .. $#QUERIES ) {
    push @COOKIES, cookie(-name=>"RunNumber:$QUERIES[$i]{'name'}", -value=> \%{ $QUERIES[$i]{'value'} }, -expires=>'+180d');
  }

  print header(-cookie=>\@COOKIES),
        start_html( -title=>'ATLAS Run Numbers'),
	start_form;

  @TIME_ZONE_NAMES = ('UTC');
  push @TIME_ZONE_NAMES, 'CERN';

  @result = `./rn_table.sh $tdaq_release foo bar -z list-regions`;
  $status=$? >> 8;

  if($status != 0) {
    for $i ( 0 .. $#result ) {
      print span({-style=>'color: red;'},$result[$i],"<br/>");
    }
    &sign();
    return 0;
  }

  for $i ( 0 .. $#result ) {
    $z=$result[$i];
    $z =~ s/^\s*(.*?)\s*$/$1/;
    push @TIME_ZONE_NAMES,"$z";
  }

  #print '<div style="text-align: right;"><nst class="TZ">Select timezone: </nst>', popup_menu(-class=>'TZ', -name=>'tmz', -values=>\@TIME_ZONE_NAMES, -default=>"$tz_name" ), '</div>', p;

  $db_name='';

  if(param()) {
    $db_name=param('db');

    if($db_name eq "") {
      $db_name=param('dbh');
    }
    
    if(param('btn0')) {
      $db_name=$parameters{'dbh'};
    }

    if($db_name eq "") {
      print
        h1('ATLAS Run Numbers'),
        'Select database: ', popup_menu(-name=>'db', -values=> [ @DBNAMES ], -default=>'' ), p, submit('btn1','Select'), hr,
        span({-style=>'color: red;'},'<b>ERROR: Select a database!</b>');
      &sign();
      return 0;
    }
  }

  if("$db_name" eq "") {
    print
      h1('ATLAS Run Numbers'),
      "Select database to prepare new request: ", popup_menu(-name=>'db', -values=> [ @DBNAMES ], -default=>'' ), p, submit('btn2','Select');

    if ($#QUERIES) {
      for $i ( 0 .. $#QUERIES ) {
        if("$QUERIES[$i]{'value'}{'dbh'}" ne "") {
	  $s='runs stored on ';

	  if($QUERIES[$i]{'value'}{'names'} ne "*") {$s .= "\"$QUERIES[$i]{'value'}{'names'}\"@";}

	  $s .= "\"$QUERIES[$i]{'value'}{'dbh'}\" DB ";

          if(($QUERIES[$i]{'value'}{'started_since'} ne "") || ($QUERIES[$i]{'value'}{'started_until'} ne "")) {
            $s .= 'started ';
	    if($QUERIES[$i]{'value'}{'started_since'} ne "") {$s .= "since \"$QUERIES[$i]{'value'}{'started_since'}\" ";}
	    if($QUERIES[$i]{'value'}{'started_until'} ne "") {$s .= "till \"$QUERIES[$i]{'value'}{'started_until'}\" ";}
          }

          if(($QUERIES[$i]{'value'}{'min_dur'} ne "") || ($QUERIES[$i]{'value'}{'max_dur'} ne "")) {
            $s .= 'lasted ';
	    if   ($QUERIES[$i]{'value'}{'min_dur'} eq "") {$s .= "within \"$QUERIES[$i]{'value'}{'max_dur'}\" ";                             }
	    elsif($QUERIES[$i]{'value'}{'max_dur'} eq "") {$s .= "longer \"$QUERIES[$i]{'value'}{'min_dur'}\" ";                             }
	    else                                          {$s .= "between \"$QUERIES[$i]{'value'}{'min_dur'}\" and \"$QUERIES[$i]{'value'}{'max_dur'}\" ";}
          }

          if(($QUERIES[$i]{'value'}{'min_rn'} ne "") || ($QUERIES[$i]{'value'}{'max_rn'} ne "")) {
            $s .= 'run numbers ';
	    if   ($QUERIES[$i]{'value'}{'min_rn'} eq "") {$s .= "below \"$QUERIES[$i]{'value'}{'max_rn'}\" ";                             }
	    elsif($QUERIES[$i]{'value'}{'max_rn'} eq "") {$s .= "above \"$QUERIES[$i]{'value'}{'min_rn'}\" ";                             }
	    else                                         {$s .= "between \"$QUERIES[$i]{'value'}{'min_rn'}\" and \"$QUERIES[$i]{'value'}{'max_rn'}\" ";}
          }

          if ($QUERIES[$i]{'value'}{'partition'} ne "") {
            $s .= "taken in \"$QUERIES[$i]{'value'}{'partition'}\" partition ";
          }

          if ($QUERIES[$i]{'value'}{'release'} ne "") {
            $s .= "using \"$QUERIES[$i]{'value'}{'release'}\" release ";
          }

          if ($QUERIES[$i]{'value'}{'file'} ne "") {
            $s .= "configured by \"$QUERIES[$i]{'value'}{'file'}\" file ";
          }

          if(($QUERIES[$i]{'value'}{'host'} ne "") || ($QUERIES[$i]{'value'}{'user'} ne "")) {
            $s .= 'archived ';
	    if($QUERIES[$i]{'value'}{'host'} ne "") {$s .= "on host \"$QUERIES[$i]{'value'}{'host'}\" ";}
	    if($QUERIES[$i]{'value'}{'user'} ne "") {$s .= "by \"$QUERIES[$i]{'value'}{'user'}\" ";}
          }

          push @LABEL_VALUES, $i;
          $LABELS{"$i"}=$s;
        }
      }

      if($#LABEL_VALUES > -1) {
        print h2('Recent Requests'),
	      radio_group(-name => 'selected_request', -values => [ @LABEL_VALUES ], -linebreak => 'true', -labels => \%LABELS),
	      p, submit('btn0','Run Query');
      }
    }

    &sign();
    return 0;
  }
  else {
    print h1("ATLAS Run Numbers for \"$db_name\" database");
    print hidden(-name=>'dbh', -default=> [$db_name]);
  }

  for $i ( 0 .. $#DB ) {
    $v=$DB[$i]{"name"};
    if ($db_name eq "$v") {
      $db_name=$v;
      $connect_as=$DB[$i]{"connect_as"};
      $db_owner=$DB[$i]{"db_owner"};
      $oks_connect_as=$DB[$i]{"oks_connect_as"};
      $oks_db_owner=$DB[$i]{"oks_db_owner"};
      $git_base_url=$DB[$i]{"git_base_url"};
    }
  }

  $got_list_runs=0;
  
  @NAMES=param('hn');

  if($#NAMES < 1) {
    push @NAMES, '*';
    @result = `./rn_table.sh $tdaq_release $connect_as $db_owner -l`;
    $status=$? >> 8;

    if($status != 0) {
      for $i ( 0 .. $#result ) {
        print span({-style=>'color: red;'},$result[$i],"<br/>");
      }
      &sign();
      return 0;
    }

    for $i ( 0 .. $#result ) {
      if ($result[$i] =~ /^Found .*/) { next; }
      $result[$i] =~ s/ - //;
      $result[$i] =~ s/\'//g;
      $result[$i] =~ s/^\s*(.*?)\s*$/$1/;
      push @NAMES, $result[$i];
    }
  }
  
  if(!param('btn3') && !param('btn4') && !param('btn0')) {
    @DN = ();

    for $i ( 0 .. $#DURATIONS ) {
      push @DN, $DURATIONS[$i]{"name"};
    }

    print
      "Show runs between now and ", popup_menu(-name=>'time_int', -values=> [ @INTERVALS ] ), " ago", p,
      "Show runs lasted from ", popup_menu(-name=>'mnd', -values=> [ @DN ] ), " to ", popup_menu(-name=>'mxd', -values=> [ @DN ] ), p;
  }

  print
    "Select run database name: ", popup_menu(-name=>'names', -values=> [ @NAMES ], -default=>"$parameters{'names'}" ), p,
    hidden(-name=>'hn', -default=> [ @NAMES ]);

  if(!param('btn3') && !param('btn4') && !param('btn0')) {
    print submit('btn3','Run Query');
    &sign();
    return 0;
  }

  $rn_db_name=''; $x=$parameters{'names'};
  if ("$x" ne "*")  {
    $rn_db_name="-d $x";
    print hidden(-name=>'rnh', -default=> [ $x ]);
  }

  if("$tz_name" eq "" || "$tz_name" eq "UTC") { $tz_table_title = 'UTC';              $tz_var= '';                 $tz_pfx='';            }
  elsif("$tz_name" eq "CERN")                 { $tz_table_title = 'CERN local time';  $tz_var= '-z Europe/Zurich'; $tz_pfx=' local time'; }
  else                                        { $tz_table_title = 'local time';       $tz_var= "-z $tz_name";      $tz_pfx=' local time'; }

  $tm_ref="<i>  ($tz_name$tz_pfx, use <a href='http://www.iso.org/iso/en/prods-services/popstds/datesandtime.html'>ISO 8601</a> date-time format)</i>";
  $in_ref='<i>  (use HH:MM:SS time duration format)</i>';

  print
    'Show runs started since ', textfield('started_since',$parameters{'started_since'},19,20), " until ", textfield('started_until',$parameters{'started_until'},19,20), "$tm_ref", p,
    'Show runs lasted from ', textfield('min_dur',$parameters{'min_dur'},7,9), " to ", textfield('max_dur',$parameters{'max_dur'},7,9), "$in_ref", p,
    'Show run numbers from ', textfield('min_rn',$parameters{'min_rn'},5,8), " to ", textfield('max_rn',$parameters{'max_rn'},5,8),
    '<br/>&nbsp;&nbsp;<font size="-1"><i>(leave above fields empty to be ignored by query)</i></font>', p,
    'Show user ', textfield('user',$parameters{'user'},12), ' host ', textfield('host',$parameters{'host'},16), ' partition ', textfield('partition',$parameters{'partition'},20),
      , ' TDAQ release ', textfield('release',$parameters{'release'},13), , ' config file ', textfield('file',$parameters{'file'},32),
    '<br/>&nbsp;&nbsp;<font size="-1"><i>(leave a field empty to be ignored, or put exact name, or use expression with <a href="#wildcards">wildcards</a>)</i></font>', p,
    h3('User preferences'), p,
    'Select timezone: ', popup_menu(-name=>'tmz', -values=>\@TIME_ZONE_NAMES, -default=>"$tz_name" ), p,
    'Select optional table columns: ';

  $show_release=$table_details_c{'release'};
  $show_user=$table_details_c{'user'};
  $show_host=$table_details_c{'host'};
  $show_config=$table_details_c{'config'};
  $show_file=$table_details_c{'file'};
  $show_comments=$table_details_c{'comments'};

  @tabel_details_selected = ();
  foreach (@EXTRA_TABLE_COLUMNS) {
    if($table_details_c{$_} eq 'yes') { push @tabel_details_selected, "$_"; }
  }

  print
    checkbox_group(-name=>'table_details',
                   -values=>[@EXTRA_TABLE_COLUMNS],
                   -defaults=>[@tabel_details_selected]), p,
    submit('btn4','Run Query'), ' ', defaults('New Query'), hr;


  if (param()) {
     $x=$parameters{'started_since'}; if ("$x" ne "")  { $x =~ s/ /T/g; $sft="-s $x"; } 
     $x=$parameters{'started_until'}; if ("$x" ne "")  { $x =~ s/ /T/g; $stt="-t $x"; } 

     $x=$parameters{'min_dur'}; if ( ("$x" ne "") &&  ("$x" ne "*") ) { $min_dur_ti="-f $x"; } 
     $x=$parameters{'max_dur'}; if ( ("$x" ne "") && ("$x" ne "*") ) { $max_dur_ti="-i $x"; } 

     $min_rn=''; $x=$parameters{'min_rn'}; if ("$x" ne "") { $min_rn="-n $x"; } 
     $max_rn=''; $x=$parameters{'max_rn'}; if ("$x" ne "") { $max_rn="-m $x"; }

     $host_p=''; $x=$parameters{'host'}; if ("$x" ne "") { $host_p="-o $x" }
     $user_p=''; $x=$parameters{'user'}; if ("$x" ne "") { $user_p="-e $x" }
     $part_p=''; $x=$parameters{'partition'}; if ("$x" ne "") { $part_p="-p $x" }
     $release_p=''; $x=$parameters{'release'}; if ("$x" ne "") { $release_p="-r $x" }
     $file_p=''; $x=$parameters{'file'}; if ("$x" ne "") { $file_p="-a $x" }

     print h2('Run Numbers History');

     @result = `./rn_table.sh $tdaq_release $connect_as $db_owner $rn_db_name $min_rn $max_rn $sft $stt $min_dur_ti $max_dur_ti $host_p $user_p $part_p $release_p $file_p $tz_var`;
     $status=$? >> 8;

     if($status != 0) {
       for $i ( 0 .. $#result ) {
         print span({-style=>'color: red;'},$result[$i],"<br/>");
       }
       &sign();
       return 0;
     }

     $table_title_bg_color="#99CCFF";
     $bg_color="#F0F0F0";

     print "\n<table border=\"1\" bgcolor=\"$bg_color\" style=\"border-collapse: collapse\">\n",
            "<tr>",
	     "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Name</th>",
	     "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Number</th>",
	     "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Start At ($tz_table_title)</th>",
	     "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Duration</th>",
	     "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Partition</th>";

     if($show_release eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Release</th>";
     }

     if($show_user eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">User</th>";
     }

     if($show_host eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Host</th>";
     }

     if($show_config eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Config</th>";
     }

     if($show_file eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">File</th>";
     }

     if($show_comments eq 'yes') {
       print "<th nowrap=\"nowrap\" bgcolor=\"$table_title_bg_color\">Comments</th>";
     }

     print "</tr>\n";

     for $i ( 0 .. $#result ) {
       if ($result[$i] =~ /^=.*/) { next; }

       ($x,$name, $num, $sta, $spa, $rel, $user, $host, $part, $config, $file, $comments) = split(/\|/,$result[$i]);

       $name =~ s/^\s*(.*?)\s*$/$1/;
       $num =~ s/ //g;
       $sta =~ s/^\s*(.*?)\s*$/$1/;
       $spa =~ s/^\s*(.*?)\s*$/$1/;
       $part =~ s/^\s*(.*?)\s*$/$1/;

       if ($num eq "Num" || $num eq "") { next; }   # skip table's caption

       print
         "<tr align=\"left\">",
          "<td>$name</td>",
	  "<td align=\"right\">$num &nbsp;</td>",
	  "<td>$sta</td>",
	  "<td align=\"right\">$spa &nbsp;</td>",
	  "<td>$part</td>";

       $rel =~ s/ //g;
       if($show_release eq 'yes') {
         print "<td>$rel</td>";
       }

       if($show_user eq 'yes') {
         $user =~ s/ //g;
         print "<td>$user</td>";
       }

       if($show_host eq 'yes') {
         $host =~ s/ //g;
         print "<td>$host</td>";
       }

       $hash='';
       $config =~ s/ //g;
       if ("$config" ne "" && index($config, 'hash:') == 0) {
         $hash=substr($config, 5);
       }

       if($show_config eq 'yes') {
         if ("$config" ne "") {
           if ("$hash" ne "") {
             $short=substr($hash, 0, 10);
             $config = "<a href=\"$git_base_url/$rel/-/tree/$hash\">$short</a>";
           }
	   else {
	     ($sv, $dv) = split(/\./,$config);
	     $config = "<a href=\"getdata.sh?$tdaq_release&$oks_connect_as&$oks_db_owner&$sv&$dv&$part\">$config</a>";
	   }
         }
         print "<td>$config</td>";
       }

       if($show_file eq 'yes') {
         $file =~ s/ //g;
         if("$hash" ne "") {
           $file = "<a href=\"$git_base_url/$rel/-/blob/$hash/$file\">$file</a>";
         }
         print "<td>$file</td>";
       }

       if($show_comments eq 'yes') {
         $comments =~ s/^\s*(.*?)\s*$/$1/;
         print "<td align=\"left\">$comments</td>";
       }

       print "</tr>\n";
     }

    print "</table>\n",hr;


  }

  print h2('Help'),
      h3('Expression with wildcards'),
      "<a name=\"wildcards\"></a>To provide a name one can use expression with wildcards, where:",
      "<blockquote><table border=\"0\">",
      "<tr> <td><b>%</b> (i.e. percent symbol)</td> <td>- replaces any string of zero or more characters</td> </tr>",
      "<tr> <td><b>_</b> (i.e. underscore symbol)</td> <td>- replaces any single character</td> </tr>",
      "</table></blockquote>",
      hr;

  &sign();

sub sign {
  $now_string = localtime;
  print end_form, hr,
        "\n<i><font size=\"-1\">Generated automatically at $now_string. ",
        "Report bugs to <a href=\"http://consult.cern.ch/xwho/people/432778\">author</a>.",
        "</font></i>\n",
        end_html;
}
