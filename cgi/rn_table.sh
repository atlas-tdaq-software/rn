#!/bin/sh

TNS_ADMIN=/afs/cern.ch/project/oracle/admin
export TNS_ADMIN

# setup tdaq release

source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r $1 >/dev/null || (echo "Failed to setup $1 DAQ/HLT-I release"; exit 1)

CORAL_AUTH_PATH='/afs/cern.ch/atlas/project/tdaq/web/cgi'
export CORAL_AUTH_PATH

# get table

cmd_args="-c $2 -w $3"

shift
shift
shift

rn_ls ${cmd_args} $* 2>&1 || (echo "<b>rn_ls</b> failed: $?"; exit 2)

exit $?
