#include <ers/ers.h>
#include <ipc/core.h>

#include <stdlib.h>
#include <rn/rn.h>

static void
no_param(const char * cp)
{
  std::cerr << "no parameter(s) for argument \"" << cp << "\" been provided";
}

int main(int argc, char * argv[])
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue & ex)
    {
      ers::warning(ers::Message(ERS_HERE, ex));
    }

  std::string connect;
  std::string partition;
  unsigned long run_number = 0;
  bool bad_exit = false;

  for(int i = 1; i < argc; i++) {
    const char * cp = argv[i];

    if(!strcmp(cp, "-h") || !strcmp(cp, "--help")) {
      std::cout <<
        "Usage: rn_ls\n"
        "       -c | --connect-string connect_string\n"
        "       -p | --partition partition_name\n"
        "       [-n | --existing-run-number run_number]\n"
        "       [-h | --help]\n"
        "\n"
        "Options/Arguments:\n"
        "       -c connect_string    database connection string\n"
        "       -p partition_name    name of the partition to work on\n" 
        "       -n run_number        reconnect to existing run number\n"
        "       -x                   exit without proper destruction of run number object\n"
        "       -h                   print this message\n"
        "\n"
        "Description:\n"
        "       It is a test for run number service.\n";

      return (EXIT_SUCCESS);
    }
    else if(!strcmp(cp, "-c") || !strcmp(cp, "--connect-string")) {
      if(++i == argc) { no_param(cp); return (EXIT_FAILURE); } else { connect = argv[i]; }
    }
    else if(!strcmp(cp, "-p") || !strcmp(cp, "--partition")) {
      if(++i == argc) { no_param(cp); return (EXIT_FAILURE); } else { partition = argv[i]; }
    }
    else if(!strcmp(cp, "-n") || !strcmp(cp, "--existing-run-number")) {
      if(++i == argc) { no_param(cp); return (EXIT_FAILURE); } else { run_number = atol(argv[i]); }
    }
    else if(!strcmp(cp, "-x") || !strcmp(cp, "--bad-exit")) {
      bad_exit = true;
    }
    else {
      std::cerr << "Unexpected parameter \"" << cp << '\"';
      return (EXIT_FAILURE);
    }
  }

  try {
    if(!getenv("TDAQ_DB_DATA"))
      setenv("TDAQ_DB_DATA", "dummy.data.xml", 1);

    tdaq::RunNumber test(connect, partition, run_number);

    if(unsigned long run_number = test.get_number()) {
      std::cout << "New run number is " << run_number << std::endl;
    }
    else {
      std::cerr << "ERROR: failed to get new run number" << std::endl;
      return 1;
    }

    std::cout << "Sleep 2 seconds ..." << std::endl;
    sleep(2);

    if(bad_exit) exit(0);

    test.set_comments((run_number ? "Test reconnect to existing run." : "It is just a test."));

    test.close();  // avoid possible exception in destructor
  }

  catch (ers::Issue &ex) {
    ers::error(ex);
    return 2;
  }

  ERS_DEBUG(1, "Exiting ...");
  return 0;
}
